<?php
require_once('../config.php');
require_once(BASE_PATH . '/dal/basic_dal.php');
require_once(BASE_PATH . '/logic/posts.php');
require_once(BASE_PATH . '/logic/tags.php');
require_once(BASE_PATH . '/logic/categories.php');
function getUserId()
{
    if (session_status() != PHP_SESSION_ACTIVE) session_start();
    if (isset($_SESSION['user'])) return $_SESSION['user']['id'];
    return 0;
}

$post_id = $_REQUEST['id'];
$SQL = "SELECT * FROM `posts` JOIN categories c ON c.id = posts.category_id WHERE posts.id = ".$post_id;
$result = getRow($SQL);
$tags = getTags();
$categories = getCategories();
$sql_tags = "SELECT * FROM posts as p JOIN post_tags as t ON p.id = t.post_id JOIN tags ON tags.id = t.tag_id WHERE p.id = ".$post_id;

$temp_selected_tages = getRows($sql_tags);
$selected_tages = [''];
for($i = 0; $i < count($temp_selected_tages); $i++) {
    array_push($selected_tages, $temp_selected_tages[$i]['name']);
}
if (isset($_REQUEST)) {
    $errors = validatePostCreate($_REQUEST);
    if (count($errors) == 0) {
        if(($_FILES)) {
            if (updatePost($_REQUEST, getUserId(), getUploadedImage($_FILES))) {
                header('Location:index.php');
                die();
            } else {
                $generic_error = "Error while adding the post";
            }
        }
    }
}

require_once(BASE_PATH . '/layout/header.php');
?>
<!-- Page Content -->
<!-- Banner Starts Here -->
<div class="heading-page header-text">
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-content">
                        <h4>Edit Post</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Banner Ends Here -->
<section class="blog-posts">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="all-blog-posts">
                    <div class="row">
                        <div class="col-sm-12">
                            <form method="POST" enctype="multipart/form-data">
                                <textarea name="title" class="form-control" style='color:blue;'><?=$result['title']?></textarea>
                                <?= isset($errors['title']) ? "<span class='text-danger'>" . $errors['title'] . "</span>" : "" ?>
                                
                                <textarea name="content" class="form-control" style='color:blue;'><?=$result['content']?></textarea>
                                <?= isset($errors['content']) ? "<span class='text-danger'>" . $errors['content'] . "</span>" : "" ?>
                                
                                <label>Upload Different Image <input type="file" name="image" /></label><br />
                                <?= isset($errors['image']) ? "<span class='text-danger'>" . $errors['image'] . "</span>" : "" ?>

                                <label>Publish date<input type="date" name="publish_date" class="form-control"></label>
                                <?= isset($errors['publish_date']) ? "<span class='text-danger'>" . $errors['publish_date'] . "</span>" : "" ?>
                                <select name="category_id" class="form-control">
                                    <option value=""><?=$result['name']?></option>
                                    <?php
                                    foreach ($categories as $category) {
                                        if($category['name'] != $result['name']) {
                                            echo "<option value='{$category['id']}'>{$category['name']}</option>";
                                        }  
                                    }
                                    ?>
                                </select>
                                <?= isset($errors['category_id']) ? "<span class='text-danger'>" . $errors['category_id'] . "</span>" : "" ?>
                                <select name="tags[]" multiple class="form-control">
                                    <?php
                                    foreach ($tags as $tag) {
                                        if(array_search($tag['name'], $selected_tages)) {
                                            echo "<option style='color:blue;' value='{$tag['id']}'>{$tag['name']}</option>";
                                        }   
                                        else {
                                            echo "<option value='{$tag['id']}'>{$tag['name']}</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <?= isset($errors['tags']) ? "<span class='text-danger'>" . $errors['tags'] . "</span>" : "" ?>
                                <button class="btn btn-success">Edit Post</button>
                                <a href="index.php" class="btn btn-danger">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once(BASE_PATH . '/layout/footer.php') ?>